#!/bin/bash

# Get old version for be used as cache
docker pull $CI_REGISTRY_IMAGE:$STAGE_ENVIRONMENT || true

# Build it from cache
docker build --cache-from $CI_REGISTRY_IMAGE:$STAGE_ENVIRONMENT \
--tag $CI_REGISTRY_IMAGE:$STAGE_ENVIRONMENT \
-f $DOCKER_FILE .

# Push images
docker push $CI_REGISTRY_IMAGE:$STAGE_ENVIRONMENT
